package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/mux"
	ws "github.com/gorilla/websocket"
	"github.com/vmihailenco/msgpack"
)

type Package struct {
	From string
	To   []string
}

var upgater = ws.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}
var conns = map[string][]*conn{}

type conn struct {
	conn     *ws.Conn
	sendLock sync.Mutex
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/connect", func(w http.ResponseWriter, r *http.Request) {
		uuid := r.Header.Get("UUID")
		if uuid == "" {
			log.Println("Couldn't get uuid")
			http.Error(w, "Valid UUID required. ", 400)
			return
		}
		wsconn := &conn{}
		var err error
		wsconn.conn, err = upgater.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("Couldn't upgrade to websocket: %v", err)
			http.Error(w, "Couldn't upgrade to websocket. ", http.StatusInternalServerError)
			return
		}

		log.Printf("New Connection: %s", uuid)

		conns[uuid] = append(conns[uuid], wsconn)

		for {
			_, r, err := wsconn.conn.NextReader()
			if err != nil {
				log.Printf("Couldn't read from websocket: %v", err)
				break
			}
			var buf bytes.Buffer
			var pkg Package
			if err := msgpack.NewDecoder(io.TeeReader(r, &buf)).Decode(&pkg); err != nil {
				log.Printf("Error decoding package: %v", err)
			}
			log.Printf("New Message: %#v\n", pkg)

			//var unavail []string
			for _, f := range pkg.To {
				rconn, ok := conns[f]
				if !ok {
					//unavail = append(unavail, f)
					continue
				}
				for _, c := range rconn {
					c.sendRaw(buf.Bytes())
				}
			}
			/*wsconn.send(model.Package{
				ID:      msg.ID,
				Type:    "status",
				From:    wsconn.footprint,
				To:      unavail,
				Content: "unavail",
			})*/
		}
	})

	log.Println("Start Listening...")

	srv := http.Server{
		Addr:    ":8888",
		Handler: r,
	}

	log.Fatalln(srv.ListenAndServe())
}

func (c *conn) sendRaw(raw []byte) error {
	c.sendLock.Lock()
	defer c.sendLock.Unlock()
	w, err := c.conn.NextWriter(ws.TextMessage)
	if err != nil {
		return fmt.Errorf("Couldn't send message: %v", err)
	}

	defer w.Close()
	_, err = w.Write(raw)
	return err
}

func Keys(m map[string]*conn) []string {
	var ret []string
	for k := range m {
		ret = append(ret, k)
	}
	return ret
}
